from django.urls import path
from . import views
from .views import books, data

app_name = 'story9'

urlpatterns = [
    path('books/', views.books, name='books'),
    path('data/', data, name='books_data'),
    # dilanjutkan ...
]