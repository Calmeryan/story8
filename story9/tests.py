from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import books

# Create your tests here.
class Story9Test(TestCase):
    def test_story9_page_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_page_is_used(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response,'books.html')

    def test_json(self):
        response = Client().get('/books/data/')
        self.assertEqual(response.status_code,200)