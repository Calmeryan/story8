from django.shortcuts import render
from django.http import JsonResponse
import json
import requests
from .models import Book
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def books(request):
    return render(request, 'books.html')

def data(request):
    try:
        q = request.GET['q']
    except:
        q = "quilting"
    
    json_read = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + q).json()

    return JsonResponse(json_read)