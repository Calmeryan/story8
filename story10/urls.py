from django.urls import path
from . import views

app_name = 'story10'

urlpatterns = [
    path('story10/', views.index, name='index'),
    path('signup/', views.signup,name='signup'),
    path('main/', views.main,name='main'),
    path('logout/',views.logout,name='logout')
]