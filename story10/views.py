from django.shortcuts import render, redirect
from django.http import JsonResponse
import json
import requests
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib import messages
from django.urls import reverse
from django.contrib.auth import logout as django_logout

# Create your views here.

def index(request):

	if(request.method=='GET'):

		return render(request, 'story10.html')
	else:
		username=request.POST.get("Username")
		password=request.POST.get("Password")

		user=authenticate(username=username,password=password)
		if(user is not None):
			login(request,user)
			return redirect('story10:main')
		messages.error(request,"Incorrect username/password")
		return render(request,'story10.html')

def signup(request):

	if(request.method == 'GET'):
			return render(request, 'Account.html')
		
	else:
		username= request.POST.get("Username")
		password=request.POST.get("Password")
		try:
			user = User.objects.get(username=username)
			messages.error(request,"Sorry username is already taken :(")
			return render(request,'Account.html')
		except:
			user=User.objects.create_user(username=username,password=password)
			messages.success(request,"You have succesfully created a new account")

			
			return redirect(reverse('story10:index'))

@login_required(login_url='story10:index')
def main(request):
	if(request.method=='GET'):
		return render(request,'Logout.html')

def logout(request):
	django_logout(request)
	return redirect('story10:index')