from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from django.test import LiveServerTestCase
from django.test import TestCase, Client
from django.urls import resolve


# Create your tests here.
class UnitTest(TestCase):
    def test_page_story10_exist(self):
        response = Client().get('/story10/story10/')
        self.assertEqual(response.status_code, 200)

    def test_page_Logout_exist(self):
        response = Client().get('/story10/signup/')
        self.assertEqual(response.status_code, 200)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')     
        chrome_options.add_argument('--disable-dev-shm-usage')           
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def check_functional(self):
        selenium = self.selenium
        self.selenium.get(self.live_server_url + '/')
        time.sleep(3)

        btn_story10 = selenium.find_element_by_name('btn-story10')
        btn_story10.send_keys(Keys.RETURN)
        time.sleep(3)

        signbut = selenium.find_element_by_name('signbut')
        signbut.send_keys(Keys.RETURN)
        time.sleep(3)

        Username = selenium.find_element_by_name('Username')
        Password = selenium.find_element_by_name('Password')

        Username.send_keys("Test1")
        Password.send_keys("abcd")

        create = selenium.find_element_by_name('signup')
        create.send_keys(Keys.RETURN)

        time.sleep(3)

        log_user = selenium.find_element_by_name('Username')
        log_pass = selenium.find_element_by_name('Password')

        Username.send_keys("Test1")
        Password.send_keys("abcd")

        loginL=selenium.find_element_by_name('login')
        loginL.send_keys(Keys.RETURN)
        time.sleep(3)

        Logoutbut= selenium.find_element_by_name('logout')
        Logoutbut.send_keys(Keys.RETURN)














