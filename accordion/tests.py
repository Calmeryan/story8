from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import index
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class UnitTest(TestCase):
    def test_landing_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story8_use_func_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def functional_test(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/')
        time.sleep(3)

        accord = self.selenium.find_elements_by_class_name('accordion')
        self.assertEqual(len(accord), 4)
        time.sleep(3)

        accord1 = selenium.find_element_by_name('aktivitas')
        accord1.send_keys(Keys.RETURN)
        time.sleep(3)

        accord2 = selenium.find_element_by_name('pengalaman')
        accord2.send_keys(Keys.RETURN)
        time.sleep(3)

        up2 = selenium.find_element_by_name('up2')
        up2.send_keys(Keys.RETURN)
        time.sleep(2)

        down2 = selenium.find_element_by_name('down2')
        down2.send_keys(Keys.RETURN)
        time.sleep(3)