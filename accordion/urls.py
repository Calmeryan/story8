from django.urls import path
from . import views

appname = 'accordion'

urlpatterns = [
    path('', views.index, name='index'),
    path('books/', views.story9, name='story9'),
    path('story10/', views.story10, name='story10')
]