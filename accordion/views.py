from django.shortcuts import render, redirect
from django.urls import reverse

# Create your views here.
def index(request):
    return render(request, 'landing.html')

def story9(request):
    return render(request, 'books.html')

def story10(request):
    return redirect(reverse('story10:index'))